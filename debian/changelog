python-softlayer (6.2.5-1) unstable; urgency=medium

  * Team upload.
  * Use new dh-sequence-python3
  * Remove Py2 vs 3 hybridation
  * Autopkgtest does not need python3-mock anymore
  * Reformat d/control with debputy

  [ Bo YU ]
  * New upstream version 6.2.5
  * Refresh 0001-Rename-launcher-script-to-avoid-namespace-conflicts.patch
  * Bump std-ver to 4.7.0
  * Update year of d/copyright
  * Add more data files for autopkgtest

 -- Alexandre Detiste <tchet@debian.org>  Fri, 21 Feb 2025 21:38:55 +0100

python-softlayer (6.1.4-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse,
    Security-Contact.
  * Update standards version to 4.6.2, no changes needed.

  [ Bo YU ]
  * New upstream version 6.1.4

 -- Bo YU <tsu.yubo@gmail.com>  Mon, 27 Feb 2023 12:16:49 +0800

python-softlayer (6.1.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 6.1.3
    Fix ftbfs on python3.11. Closes: #1024877
  * Rebase d/patch
  * Set std-ver to 4.6.1
  * Update d/copyright
  * Remove unnecessary depends for binary package
  * Add python3-zeep for autopkgtest
  * Adjust autopkgtest
  * Update d/watch

 -- Bo YU <tsu.yubo@gmail.com>  Mon, 05 Dec 2022 10:21:05 +0000

python-softlayer (5.9.8-2) unstable; urgency=medium

  * Integrate Python Team changes
  * Remove patch applied upstream

 -- Ana Custura <ana@netstat.org.uk>  Mon, 07 Feb 2022 10:29:47 +0000

python-softlayer (5.9.8-1) unstable; urgency=medium

  * New upstream version 5.9.8
  * Bump debhelper-compat version to 13
  * Add Rules-Requires-Root: no to debian/control

 -- Ana Custura <ana@netstat.org.uk>  Mon, 07 Feb 2022 10:24:46 +0000

python-softlayer (5.9.7-1) unstable; urgency=medium

  * New upstream release; Closes: #997878
  * debian/patches/
    - refresh, drop patch released upstream
  * debian/patches/collections-abc-mapping.patch
    - fix a test failure by importing Mapping from the right module
  * debian/control
    - run wrap-and-sort
  * debian/tests/control
    - install tkinter, needed by autopkgtests

 -- Sandro Tosi <morph@debian.org>  Sun, 21 Nov 2021 01:15:20 -0500

python-softlayer (5.8.5-2) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Graham Inggs ]
  * Fix testing domain that should not exist (Closes: #989620)
  * Add needs-internet autopkgtest restriction

 -- Graham Inggs <ginggs@debian.org>  Fri, 11 Jun 2021 09:35:36 +0000

python-softlayer (5.8.5-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Gordon Ball ]
  * New upstream version 5.8.5 (closes: #948397)

 -- Ana Custura <ana@netstat.org.uk>  Fri, 07 Feb 2020 11:06:13 +0000

python-softlayer (5.7.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Drop Python 2 support. Closes: #938185

  [ Sandro Tosi ]
  * Team upload.

 -- Sandro Tosi <morph@debian.org>  Sun, 22 Sep 2019 17:21:51 -0400

python-softlayer (5.7.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Ana Custura ]
  * New upstream version 5.7.2

 -- Ana Custura <ana@netstat.org.uk>  Tue, 23 Jul 2019 22:14:58 +0100

python-softlayer (5.7.1-1) experimental; urgency=medium

  [ Ana Custura ]
  * New upstream release 5.7.1
  * Add myself to uploaders (Closes: #926541)
  * debian/control
    - Remove unnecessary versioned dependencies
    - Apply cme formatting fixes
    - Bump debhelper version to 12

 -- Ana Custura <ana@netstat.org.uk>  Sun, 14 Apr 2019 16:21:53 +0100

python-softlayer (5.6.4-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

  [ Scott Kitterman ]
  * New upstream release
    - Update python/python3-prettytable depends to ptable
  * Bump standards-version to 4.3.0 without further change
  * Run upstream tests as autopkgtest
  * Add ptable to pydist overrides

 -- Scott Kitterman <scott@kitterman.com>  Mon, 21 Jan 2019 13:49:47 -0500

python-softlayer (5.4.4-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.1.4 without further change
  * Drop obsolete Breaks/Replaces from python3-softlayer

 -- Scott Kitterman <scott@kitterman.com>  Mon, 30 Apr 2018 15:35:49 -0400

python-softlayer (5.4.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Scott Kitterman ]
  * New upstream release
    - Refresh patches

 -- Scott Kitterman <scott@kitterman.com>  Fri, 23 Feb 2018 22:25:41 -0500

python-softlayer (5.4.0-1) unstable; urgency=medium

  * New upstream release
  * Update debian/copyright

 -- Scott Kitterman <scott@kitterman.com>  Thu, 18 Jan 2018 18:33:07 -0500

python-softlayer (5.3.2-1) unstable; urgency=medium

  * New upstream release
  * Update debian/copyright
  * Bump standards-version to 4.1.3 without further change

 -- Scott Kitterman <scott@kitterman.com>  Tue, 09 Jan 2018 23:46:19 -0500

python-softlayer (5.2.14-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.1.1.1 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sat, 07 Oct 2017 20:19:26 -0400

python-softlayer (5.2.7-1) unstable; urgency=medium

  * New upstream release
  * Refresh patches after git-dpm to gbp pq conversion

 -- Scott Kitterman <scott@kitterman.com>  Mon, 26 Jun 2017 09:14:34 -0400

python-softlayer (5.2.1-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Tue, 08 Nov 2016 08:09:24 -0500

python-softlayer (5.2.0-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Fri, 09 Sep 2016 14:49:35 -0400

python-softlayer (5.1.0-1) unstable; urgency=medium

  * New upstream release
  * Remove uupdate from debian/watch as it is no longer useful with the
    current DPMT packaging approach
  * Bump standards version to 3.9.8 without further change
  * Add missing depends on python/python3-pygments (it had previously
    been pulled in indirectly)
  * Add version constraints for depends based on upstream requirements
  * Add missing depends on python/python3-click (it is normally pulled
    in indirectly)
  * Update debian/copyright

 -- Scott Kitterman <scott@kitterman.com>  Thu, 26 May 2016 08:31:42 -0400

python-softlayer (5.0.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)

  [ Scott Kitterman ]
  * New upstream release
  * Bump standards version to 3.9.7

 -- Scott Kitterman <scott@kitterman.com>  Sat, 02 Apr 2016 22:42:21 -0400

python-softlayer (4.1.1-2) unstable; urgency=medium

  * Fix package depends for python3-softlayer (spell python3-prompt-toolkit
    correctly)

 -- Scott Kitterman <scott@kitterman.com>  Sun, 13 Sep 2015 23:42:48 -0400

python-softlayer (4.1.1-1) unstable; urgency=medium

  * New upstream release
  * Add myself to debian/copyright for Debian packaging
  * Add python{3}-prompt-toolkit to Depends as the package uses it now

 -- Scott Kitterman <scott@kitterman.com>  Fri, 11 Sep 2015 08:09:24 -0400

python-softlayer (4.0.4-1) unstable; urgency=medium

  * New upstream release
  * Update debian/copyright
  * Add debian uupdate to debian/watch

 -- Scott Kitterman <scott@kitterman.com>  Tue, 07 Jul 2015 22:58:31 -0400

python-softlayer (4.0.3-2) unstable; urgency=medium

  * Update debian/python*-softlayer.prerm and postinst to provide alternatives
    for /usr/bin/slcli

 -- Scott Kitterman <scott@kitterman.com>  Fri, 26 Jun 2015 21:48:18 -0400

python-softlayer (4.0.3-1) unstable; urgency=medium

  * New upstream release
    - Drop debian/patches/docopt-versions.patch since docopt is no longer
      required
    - Drop depends on python-docopt and python3-docopt, no longer used
    - Drop debian/patches/1002-longdesc.patch, no longer required
    - Update debian/patches/1001-rename_sl.patch for upstream changes

 -- Scott Kitterman <scott@kitterman.com>  Mon, 22 Jun 2015 22:15:59 -0400

python-softlayer (3.3.1-1) unstable; urgency=medium

  * New upstream release
  * Bump standards version to 3.9.6 without further change

 -- Scott Kitterman <scott@kitterman.com>  Thu, 23 Apr 2015 19:34:30 -0400

python-softlayer (3.2.0-2) unstable; urgency=medium

  * Fix docopt version in requires to SL will start (Closes: #778344)
  * Fix python-softlayer postinst/postrm file names to that alternatives works
    and SL is properly provided by python-softlayer (Closes: #778279)
  * Agreed maintainer change to Debian Python Modules Team (Closes: #777337)
  * Add Vcs-* for DPMT repository

 -- Scott Kitterman <scott@kitterman.com>  Fri, 13 Feb 2015 16:13:11 -0500

python-softlayer (3.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patch set.
  * Drop dependency on python-tox, not needed at all.
  * Refresh patches.
  * Add patch to fix FTBFS.
  * Add Python 3 support.
  * Handle /usr/bin/SL via update-alternatives.

 -- Alessio Treglia <alessio@debian.org>  Mon, 14 Jul 2014 14:41:50 +0100

python-softlayer (3.1.0-1) unstable; urgency=low

  * New upstream release.

 -- Alessio Treglia <alessio@debian.org>  Mon, 28 Apr 2014 12:35:22 +0100

python-softlayer (3.0.2-1) unstable; urgency=low

  * New upstream release.
  * Update examples, example.py is no longer provided.
  * Fix debian/watch.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Tue, 11 Mar 2014 09:35:46 +0000

python-softlayer (3.0.1-1) unstable; urgency=low

  * New upstream bugfix release.

 -- Alessio Treglia <alessio@debian.org>  Sun, 24 Nov 2013 23:43:01 +0000

python-softlayer (3.0.0-1) unstable; urgency=low

  * Initial release. (Closes: #725912)

 -- Alessio Treglia <alessio@debian.org>  Wed, 09 Oct 2013 23:29:25 +0100
